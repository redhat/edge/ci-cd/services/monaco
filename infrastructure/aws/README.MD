# Automotive AWS Infrastructure, Resources and Spending
The goal of this document is to determine standards and procedures for dealing with AWS across teams within the automotive initiative. 


## Red Hat Information Controls and Regulations
### CMDB Service Impact Analysis
//TODO

### Global Data Privacy Program and Regulations
//TODO

## Available Resources
### Computing
- EC2

### Storage
- S3




## Schema
### Controls and Policies
All users should NOT be assigned IAM policies directly in order to get permissions to execute actions in the account.

Instead, the necessary IAM policies that will define the scope of the actions will be assigned to a **group**. Assigning an IAM policy directly to a user is not recommended as it doesn’t scale (if later on we have to add/remove permissions for a group of users, we would need to go one by one).

In an early stage, assigning [administrator access](https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_job-functions.html#jf_administrator) to these groups and adding some extra policies that will limit the actions the users can perform is a quick way to start working, but we should enforce the [least privilege principle](https://docs.aws.amazon.com/IAM/latest/UserGuide/best-practices.html#grant-least-privilege), in which only the _required_ permissions are granted to any individual, as soon as we can.

The following IAM policies would be applied:
- Explicit deny on the permission to create resources without the tags defined in the “Tags: Keys: Values” section.

- Explicit deny on the permission to remove the tags defined in the “Tags: Keys: Values” from a resource.

(For more information on how the AWS IAM evaluation logic works, please see [Policy evaluation logic - AWS Identity and Access Management](https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_policies_evaluation-logic.html#policy-eval-denyallow)).

### Naming
Semi-deterministic naming? I.e auto-ci-rhosa-1 , auto-ci-s3-5 etc. (based on date or other factor?) 
//TBD

### Tags
#### Custom tags:
##### Environment Tags:
- **Env**: Dev
- **Env**: Stage
- **Env**: Prod
##### Initiative Tags:
- **Team**: a-team 
- **Team**: etc...
##### Owner Tags:
- **Owner**: {KerberosID}

